/**
 * Copyright 2010 Washington University
 */
package org.nrg.net;

import java.applet.Applet;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class NetTestApplet extends Applet {
	private static final long serialVersionUID = 1L;

	private static final String TEST_HOST = "test-host";
	private static final String TEST_SERVICES = "test-services";

	private static final String AUTHORS = "Author: Kevin A. Archie <karchie@wustl.edu>";
	private static final String COPYRIGHT = "Copyright (c) 2010 Washington University";
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private static final String APPLET_INFO = AUTHORS + LINE_SEPARATOR + COPYRIGHT;
	private static final String SERVICE_SEPARATOR = ",";

	private static final String[][] PARAMETER_INFO = {
		{ TEST_HOST, "hostname", "Remote host for connectivity testing" },
		{ TEST_SERVICES, "string", "Comma-separated list of services to test" }
	};

	private final Logger logger = LoggerFactory.getLogger(NetTestApplet.class);

	private JTextArea textArea;
	private final StringBuffer contents = new StringBuffer();

	private String hostname;

	/*
	 * (non-Javadoc)
	 * @see java.applet.Applet#getAppletInfo()
	 */
	public String getAppletInfo() { return APPLET_INFO; }

	/*
	 * (non-Javadoc)
	 * @see java.applet.Applet#getParameterInfo()
	 */
	public String[][] getParameterInfo() { return PARAMETER_INFO; }

	/*
	 * (non-Javadoc)
	 * @see java.applet.Applet#init()
	 */
	public void init() {
		logger.trace("{} INIT", this);
		textArea = new JTextArea(20, 60);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		final JScrollPane scrollPane = new JScrollPane(textArea);
		this.add(scrollPane);

		hostname = getParameter(TEST_HOST);
	}

	private static final Pattern serviceSyntax = Pattern.compile("(\\d+)");

	/*
	 * (non-Javadoc)
	 * @see java.applet.Applet#start()
	 */
	public void start() {
		logger.trace("{} START", this);
		textArea.setText(contents.toString());

		for (final String serviceDesc : getParameter(TEST_SERVICES).split(SERVICE_SEPARATOR)) {
			final Matcher matcher = serviceSyntax.matcher(serviceDesc);
			matcher.matches();
			logger.trace("matching {} : {}", serviceDesc, matcher);
			final int port = Integer.parseInt(matcher.group(1));

			contents.append("Service ").append(serviceDesc).append(LINE_SEPARATOR);
			try {
				final Socket socket = new Socket(hostname, port);
				try {
					final InputStream in = socket.getInputStream();
					try {
						final Reader reader = new InputStreamReader(in);
						contents.append(" Connected: ");
						try {
							final char[] buf = new char[512];
							for (int nread = reader.read(buf); nread > -1; nread = reader.read(buf)) {
								contents.append(String.valueOf(buf, 0, nread));
							}
						} finally {
							contents.append(LINE_SEPARATOR);
							reader.close();
						}
					} finally {
						in.close();
					}
				} finally {
					socket.close();
				}
			} catch (UnknownHostException e) {
				contents.append("Unable to resolve hostname ").append(hostname);
				contents.append(e.getMessage());
				contents.append(LINE_SEPARATOR);
			} catch (IOException e) {
				contents.append("Connection error ").append(e.getMessage());
				contents.append(LINE_SEPARATOR);
			} finally {
				textArea.setText(contents.toString());
			}
		}
	}
}
